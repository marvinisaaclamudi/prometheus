#!/usr/bin/env bash

docker-compose --env-file .env.local down --remove-orphans \
    && docker-compose --env-file .env.local up -d